package tennis;

public class Joueur implements Cloneable {
	private String name;
	private int points;
	private int jeux;
	private int sets;
	private boolean avantage;

	public Joueur(String name) {
		this.name = name;
		this.points = 0;
		this.jeux = 0;
		this.sets = 0;
		this.avantage = false;
	}
	
	/******************GETTERS******************/
	public String getName() {
		return name;
	}
	public int getPoints() {
		return points;
	}
	public int getJeux() {
		return jeux;
	}
	public int getSets() {
		return sets;
	}
	public boolean isAvantage() {
		return avantage;
	}
	
	/******************SETTERS******************/
	public void setName(String name) {
		this.name = name;
	}
	public void setPoints(int points) {
		this.points = points;
	}
	public void setJeux(int jeux) {
		this.jeux = jeux;
	}
	public void setSets(int sets) {
		this.sets = sets;
	}
	public void setAvantage(boolean avantage) {
		this.avantage = avantage;
	}
	
	/**
	 * Passes les points à 15, 30 ou 40 en fonction du nombre de points actuels
	 */
	public void gagnePoint() {
		switch(points) {
			case 0:
				points = 15;
				break;
			case 15:
				points = 30;
				break;
			case 30:
				points = 40;
				break;
		}
	}
	
	/**
	 * Incrémente les points 1 à 1
	 */
	public void gagnePointDecisif() {
		points++;
	}
	
	/**
	 * Incrémente les jeux du joueur de 1, réinitialise les points et l'avantage
	 */
	public void gainJeu() {
		jeux++;
		points = 0;
		avantage = false;
	}
	
	/**
	 * Réinitialise les points et l'avantage
	 */
	public void perteJeu() {
		points = 0;
		avantage = false;
	}
	
	/**
	 * Incrémente les sets du joueur de 1, réinitialise les points, l'avantage et les jeux
	 */
	public void gainSet() {
		sets++;
		points = 0;
		avantage = false;
		jeux = 0;
	}
	
	/**
	 * Réinitialise les points, l'avantage et les jeux
	 */
	public void perteSet() {
		points = 0;
		avantage = false;
		jeux = 0;
	}
	
	/**
	 * Réinitialise les points, l'avantange, les jeux et les sets
	 */
	public void clear() {
		points = 0;
		avantage = false;
		jeux = 0;
		sets = 0;
	}
	
	/**
	 * Duplique le joueur
	 */
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (avantage ? 1231 : 1237);
		result = prime * result + jeux;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + points;
		result = prime * result + sets;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Joueur other = (Joueur) obj;
		if (avantage != other.avantage)
			return false;
		if (jeux != other.jeux)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (points != other.points)
			return false;
		if (sets != other.sets)
			return false;
		return true;
	}
}
