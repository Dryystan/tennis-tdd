package tennis;

/**
 * @author dryys
 *
 */
/**
 * @author dryys
 *
 */
public class Partie {
	private int id;
	private boolean ended = false;
	private boolean jeuDecisif = false;
	private Joueur joueur1;
	private Joueur joueur2;

	public Partie(int id) {
		this.id = id;
	}

	public Partie(int id, Joueur joueur1, Joueur joueur2) throws CloneNotSupportedException {
		this(id);
		// Copie des joueurs afin qu'il ne soient plus modifiables de l'extérieur
		this.joueur1 = (Joueur) joueur1.clone();
		this.joueur2 = (Joueur) joueur2.clone();
		// Remise à 0 des joueurs
		this.joueur1.clear();
		this.joueur2.clear();
	}

	/******************GETTERS******************/
	
	public int getId() {
		return id;
	}
	public Joueur getJoueur1() throws CloneNotSupportedException {
		return (Joueur) joueur1.clone();
	}
	public Joueur getJoueur2() throws CloneNotSupportedException {
		return (Joueur) joueur2.clone();
	}
	public boolean isEnded() {
		return ended;
	}
	public boolean isJeuDecisif() {
		return jeuDecisif;
	}
	
	
	
	/**
	 * Attribue le nombre de points nécessaire à un joueur, en fonction de la situation de la partie<br>
	 * En cas de jeu décisif, les points sont incrémentés 1 à 1 et suivent la règle associée<br>
	 * Si le joueur est à 40 point et son adversaire en-dessous de 40, alors la méthode {@link #gainJeu(Joueur) gainJeu} est appelée<br>
	 * Si les deux joueurs sont à 40 points, la méthode {@link #gainAvantage(Joueur) gainAvantage} est appelée
	 * 
	 * @param joueur Joueur gagnant le point
	 * @throws IllegalArgumentException Joueur passé en paramètre ne correspond pas à un des deux joueurs de la partie
	 */
	public void gainPoints(Joueur joueur) throws IllegalArgumentException {
		// Si la partie est terminée, on ne fait rien
		if(ended) {
			return;
		}
		if(!joueur.equals(joueur1) && !joueur.equals(joueur2)) {
			throw new IllegalArgumentException("Joueur pas dans la partie!");
		}
		
		/**
		 * On assigne les joueurs aux variables correspondantes
		 * La classe étant une boîte noire, il faut qu'on utilise les joueurs de l'instance de classe et pas ceux passés en paramètre
		 */
		Joueur j2;
		if(joueur.equals(joueur1)) {
			joueur = joueur1;
			j2 = joueur2;
		}
		else {
			joueur = joueur2;
			j2 = joueur1;
		}
		
		if(jeuDecisif) {
			joueur.gagnePointDecisif();
			
			if(joueur.getPoints() >= 7 && joueur.getPoints() > j2.getPoints() + 1) {
				gainSet(joueur);
			}
		}
		else if(joueur.getPoints() < 40) {
			joueur.gagnePoint();
		}
		else if(joueur.getPoints() == 40 && j2.getPoints() < 40) {
			gainJeu(joueur);
		}
		else {
			gainAvantage(joueur);
		}
	}
	
	/**
	 * Attribue ou supprime un avantage à un des joueurs, en fonction du progrès de la partie<br>
	 * Si le joueur n'a pas d'avantage et que son adversaire non plus, il gagne un avantage<br>
	 * Si le joueur n'a pas d'avantage et que son adversaire en a un, son adversaire pert l'avantage<br>
	 * Si le joueur a un avantage et pas son adversaire, la méthode {@link #gainJeu(Joueur) gainJeu} est appelée
	 * 
	 * @param joueur Joueur ayant gagné un point dans la phase d'avantage
	 * @throws IllegalArgumentException Joueur passé en paramètre ne correspond pas à un des deux joueurs de la partie
	 * @throws IllegalArgumentException Nombre de points du joueur pas suffisant
	 */
	public void gainAvantage(Joueur joueur) throws IllegalArgumentException {
		if(ended) {
			return;
		}
		if(!joueur.equals(joueur1) && !joueur.equals(joueur2)) {
			throw new IllegalArgumentException("Joueur pas dans la partie!");
		}
		if(joueur.getPoints() < 40) {
			throw new IllegalArgumentException("Pas assez de points :'(");
		}
		
		Joueur j2;
		if(joueur.equals(joueur1)) {
			joueur = joueur1;
			j2 = joueur2;
		}
		else {
			joueur = joueur2;
			j2 = joueur1;
		}
		
		if(joueur.isAvantage() && !j2.isAvantage()) {
			gainJeu(joueur);
		}
		else if(!joueur.isAvantage() && !j2.isAvantage()) {
			joueur.setAvantage(true);
		}
		else {
			j2.setAvantage(false);
		}
	}
	
	/**
	 * Attribue un jeu au joueur<br>
	 * Si les deux joueurs ont six jeux après attribution du jeu, la méthode {@link #jeuDecisif() jeuDecisif} est appelée<br>
	 * Si le joueur a au moins 5 jeux et que son adversaire en 1 de moins, la méthode {@link #gainSet(Joueur) gainSet} est appelée
	 * 
	 * @param joueur Joueur gagnant le jeu
	 * @throws IllegalArgumentException Joueur passé en paramètre ne correspond pas à un des deux joueurs de la partie
	 * @throws IllegalArgumentException Nombre de points du joueur pas suffisant
	 */
	public void gainJeu(Joueur joueur) throws IllegalArgumentException {
		if(ended) {
			return;
		}
		
		if(!joueur.equals(joueur1) && !joueur.equals(joueur2)) {
			throw new IllegalArgumentException("Joueur pas dans la partie!");
		}
		if(joueur.getPoints() < 40) {
			throw new IllegalArgumentException("Pas assez de points :'(");
		}
		
		Joueur j2;
		if(joueur.equals(joueur1)) {
			joueur = joueur1;
			j2 = joueur2;
		}
		else {
			joueur = joueur2;
			j2 = joueur1;
		}
		
		if(joueur.getJeux() == 5 && j2.getJeux() == 6) {
			jeuDecisif();
		}
		else if(joueur.getJeux() > j2.getJeux() + 1 && joueur.getJeux() >= 5) {
			joueur.gainJeu();
			j2.perteJeu();
			gainSet(joueur);
		}
		else {
			joueur.gainJeu();
			j2.perteJeu();
		}
	}
	
	/**
	 * Attribue un set au joueur<br>
	 * Si le joueur avait déjà un set, alors la méthode {@link #finPartie() finPartie} est appelée
	 * 
	 * @param joueur Joueur gagnant le set
	 * @throws IllegalArgumentException Joueur passé en paramètre ne correspond pas à un des deux joueurs de la partie
	 * @throws IllegalArgumentException Pas assez de jeux gagnés
	 */
	public void gainSet(Joueur joueur) throws IllegalArgumentException {
		if(ended) {
			return;
		}
		
		if(!joueur.equals(joueur1) && !joueur.equals(joueur2)) {
			throw new IllegalArgumentException("Joueur pas dans la partie!");
		}
		if(joueur.getJeux() < 6) {
			throw new IllegalArgumentException("Il faut d'abord gagner des jeux!");
		}
		
		Joueur j2;
		if(joueur.equals(joueur1)) {
			joueur = joueur1;
			j2 = joueur2;
		}
		else {
			joueur = joueur2;
			j2 = joueur1;
		}
		
		if(joueur.getSets() == 1) {
			joueur.gainSet();
			j2.perteSet();
			finPartie();
		}
		else {
			joueur.gainSet();
			j2.perteSet();
		}
		jeuDecisif = false;
	}
	
	/**
	 * Passes la partie en mode "Jeu Décisif"<br>
	 * Voir {@link #gainPoints(Joueur) gainPoints} pour plus de détails
	 */
	public void jeuDecisif() {
		if(ended) {
			return;
		}
		jeuDecisif = true;
	}
	
	/**
	 * Passes la partie en "terminée", et affiche le nombre de sets gagnants de chaque joueur<br>
	 * Les méthodes qui permettent de faire avancer la partie ne la modifie plus
	 */
	public void finPartie() {
		ended = true;
		
		System.out.println("Fin de la partie!");
		System.out.println(result());
	}
	
	/**
	 * @return Retourne le résultat actuel de la partie
	 */
	public String result() {
		return joueur1.getName() + ": " + joueur1.getSets() + " / " + joueur2.getName() + ": " + joueur2.getSets();
	}
}
