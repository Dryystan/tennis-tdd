package tennis;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class JoueurTests {
	
	Joueur joueur;
	
	@BeforeEach
	public void setup() {
		joueur = new Joueur("Charles");
	}
	
	@Test
	void testGetName() {
		assertEquals("Charles", joueur.getName());
	}

	@Test
	void testGetPoints() {
		assertEquals(0, joueur.getPoints());
	}

	@Test
	void testGetJeux() {
		assertEquals(0, joueur.getJeux());
	}

	@Test
	void testGetSets() {
		assertEquals(0, joueur.getSets());
	}

	@Test
	void testIsAvantage() {
		assertFalse(joueur.isAvantage());
	}

	@Test
	void testSetName() {
		joueur.setName("Nouveau nom");
		assertEquals("Nouveau nom", joueur.getName());
	}

	@Test
	void testSetPoints() {
		joueur.setPoints(30);
		assertEquals(30, joueur.getPoints());
	}

	@Test
	void testSetJeux() {
		joueur.setJeux(2);
		assertEquals(2, joueur.getJeux());
	}

	@Test
	void testSetSets() {
		joueur.setSets(4);
		assertEquals(4, joueur.getSets());
	}

	@Test
	void testSetAvantage() {
		joueur.setAvantage(true);
		assertTrue(joueur.isAvantage());
	}

	@Test
	void point0A15() {
		joueur.setPoints(0);
		joueur.gagnePoint();
		assertEquals(15, joueur.getPoints());
	}

	@Test
	void point15A30() {
		joueur.setPoints(15);
		joueur.gagnePoint();
		assertEquals(30, joueur.getPoints());
	}

	@Test
	void point30A40() {
		joueur.setPoints(30);
		joueur.gagnePoint();
		assertEquals(40, joueur.getPoints());
	}

	@Test
	void pointNonValide() {
		joueur.setPoints(48);
		joueur.gagnePoint();
		assertEquals(48, joueur.getPoints());
	}
	
	@Test
	void pointNonDecisif() {
		joueur.setPoints(2);
		joueur.gagnePointDecisif();
		assertEquals(3, joueur.getPoints());
	}
	
	@Test
	void gainJeu() {
		joueur.setPoints(40);
		joueur.setAvantage(true);
		joueur.gainJeu();
		assertEquals(0, joueur.getPoints());
		assertFalse(joueur.isAvantage());
		assertEquals(1, joueur.getJeux());
	}
	
	@Test
	void perteJeu() {
		joueur.setAvantage(true);
		joueur.setPoints(40);
		joueur.perteJeu();
		assertEquals(0, joueur.getPoints());
		assertEquals(0, joueur.getJeux());
		assertFalse(joueur.isAvantage());
	}
	
	@Test
	void gainSet() {
		joueur.setPoints(40);
		joueur.setAvantage(true);
		joueur.setJeux(6);
		joueur.gainSet();
		assertEquals(0, joueur.getPoints());
		assertFalse(joueur.isAvantage());
		assertEquals(0, joueur.getJeux());
		assertEquals(1, joueur.getSets());
	}
	
	@Test
	void perteSet() {
		joueur.setPoints(40);
		joueur.setAvantage(true);
		joueur.setJeux(6);
		joueur.perteSet();
		assertEquals(0, joueur.getPoints());
		assertFalse(joueur.isAvantage());
		assertEquals(0, joueur.getJeux());
		assertEquals(0, joueur.getSets());
	}
	
	@Test
	void clear() {
		joueur.setPoints(40);
		joueur.setAvantage(true);
		joueur.setJeux(6);
		joueur.setSets(1);
		joueur.clear();
		assertEquals(0, joueur.getPoints());
		assertFalse(joueur.isAvantage());
		assertEquals(0, joueur.getJeux());
		assertEquals(0, joueur.getSets());
	}
	
	@Test
	void hashCodeTest() {
		Joueur j = new Joueur("Charles");
		assertNotSame(joueur, j);
		assertEquals(joueur.hashCode(), j.hashCode());
		joueur.setAvantage(true);
		j.setAvantage(true);
		joueur.setName(null);
		j.setName(null);
		assertNotSame(joueur, j);
		assertEquals(joueur.hashCode(), j.hashCode());
	}
	
	@Test
	void equalsTest() {
		Joueur j = joueur;
		assertTrue(joueur.equals(j));
		j = null;
		assertFalse(joueur.equals(j));
		String s = "";
		assertFalse(joueur.equals(s));
		j = new Joueur("Charles");
		j.setAvantage(true);
		assertFalse(joueur.equals(j));
		j = new Joueur("Charles");
		j.setJeux(5);
		assertFalse(joueur.equals(j));
		joueur.setName(null);
		j = new Joueur("Charles");
		assertFalse(joueur.equals(j));
		joueur = new Joueur("Charles");
		j = new Joueur("blablabla");
		assertFalse(joueur.equals(j));
		j = new Joueur("Charles");
		j.setPoints(40);
		assertFalse(joueur.equals(j));
		j = new Joueur("Charles");
		j.setSets(2);
		assertFalse(joueur.equals(j));
		joueur = new Joueur("Charles");
		j = new Joueur("Charles");
		assertTrue(joueur.equals(j));
	}
}
