package tennis;

import static org.junit.jupiter.api.Assertions.*;

import java.lang.reflect.Field;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class PartieTest {

    Partie partie;
    Joueur paul = new Joueur("Paul");
    Joueur pierre = new Joueur("Pierre");

    @BeforeEach
    public void setup() throws CloneNotSupportedException {
        partie = new Partie(1, paul, pierre);
    }

    @Test
    void testGetId() {
        assertEquals(1, partie.getId());
    }

    @Test
    void getJoueur1() throws CloneNotSupportedException {
        assertEquals(paul, partie.getJoueur1());
    }
    @Test
    void getJoueur2() throws CloneNotSupportedException {
        assertEquals(pierre, partie.getJoueur2());
    }
    @Test
    void isJeuDecisif() {
    	assertFalse(partie.isJeuDecisif());
    }
    @Test
    void isEnded() {
    	assertFalse(partie.isEnded());
    }
    
    @Test
    void illegalPlayerPoints() {
    	Throwable exception = assertThrows(IllegalArgumentException.class, () -> {partie.gainPoints(new Joueur("1"));} );
    	assertEquals("Joueur pas dans la partie!", exception.getMessage());
    }
    
    @Test
	void point0A15() throws IllegalArgumentException, CloneNotSupportedException {
    	partie.gainPoints(partie.getJoueur1());
		assertEquals(15, partie.getJoueur1().getPoints());
	}

	@Test
	void point15A30() throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
		paul.setPoints(15);
    	Field reader = Partie.class.getDeclaredField("joueur1");
    	reader.setAccessible(true);
    	reader.set(partie, paul);
    	partie.gainPoints(paul);
		assertEquals(30, paul.getPoints());
	}

	@Test
	void point30A40() throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException, CloneNotSupportedException {
		paul.setPoints(30);
    	Field reader = Partie.class.getDeclaredField("joueur1");
    	reader.setAccessible(true);
    	reader.set(partie, paul);
    	partie.gainPoints(paul);
		assertEquals(40, partie.getJoueur1().getPoints());
	}
    
    @Test
    void gainJeuFromPoints() throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException, CloneNotSupportedException {
    	paul.setPoints(40);
    	Field reader = Partie.class.getDeclaredField("joueur1");
    	reader.setAccessible(true);
    	reader.set(partie, paul);
    	partie.gainPoints(paul);
    	assertEquals(1, partie.getJoueur1().getJeux());
    	assertFalse(partie.getJoueur1().isAvantage());
    	assertFalse(partie.getJoueur2().isAvantage());
    }
    
    @Test
    void gainAvantageFromPoints() throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException, CloneNotSupportedException {
    	paul.setPoints(40);
    	pierre.setPoints(40);
    	Field reader = Partie.class.getDeclaredField("joueur1");
    	reader.setAccessible(true);
    	reader.set(partie, paul);
    	reader = Partie.class.getDeclaredField("joueur2");
    	reader.setAccessible(true);
    	reader.set(partie, pierre);
    	partie.gainPoints(paul);
    	assertTrue(partie.getJoueur1().isAvantage());
    	assertFalse(partie.getJoueur2().isAvantage());
    }
    
    @Test
    void illegalPlayerAvantage() {
    	Throwable exception = assertThrows(IllegalArgumentException.class, () -> {partie.gainAvantage(new Joueur("1"));} );
    	assertEquals("Joueur pas dans la partie!", exception.getMessage());
    }
    
    @Test
    void notEnoughPointPourAvantage() {
    	Throwable exception = assertThrows(IllegalArgumentException.class, () -> {partie.gainAvantage(pierre);} );
    	assertEquals("Pas assez de points :'(", exception.getMessage());
    }
    
    @Test
    void gainAvantage() throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException, CloneNotSupportedException {
    	paul.setPoints(40);
    	Field reader = Partie.class.getDeclaredField("joueur1");
    	reader.setAccessible(true);
    	reader.set(partie, paul);
    	partie.gainAvantage(paul);
    	assertTrue(partie.getJoueur1().isAvantage());
    	assertFalse(partie.getJoueur2().isAvantage());
    }
    
    @Test
    void perteAvantage() throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException, CloneNotSupportedException {
    	pierre.setPoints(40);
    	paul.setAvantage(true);
    	Field reader = Partie.class.getDeclaredField("joueur1");
    	reader.setAccessible(true);
    	reader.set(partie, paul);
    	reader = Partie.class.getDeclaredField("joueur2");
    	reader.setAccessible(true);
    	reader.set(partie, pierre);
    	partie.gainAvantage(pierre);
    	assertFalse(partie.getJoueur1().isAvantage());
    	assertFalse(partie.getJoueur2().isAvantage());
    }
    
    @Test
    void illegalPlayerJeu() {
    	Throwable exception = assertThrows(IllegalArgumentException.class, () -> {partie.gainJeu(new Joueur("1"));} );
    	assertEquals("Joueur pas dans la partie!", exception.getMessage());
    }
    
    @Test
    void notEnoughPointPourJeu() {
    	Throwable exception = assertThrows(IllegalArgumentException.class, () -> {partie.gainJeu(partie.getJoueur2());} );
    	assertEquals("Pas assez de points :'(", exception.getMessage());
    }
    
    @Test
    void gainJeuFromAvantage() throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException, CloneNotSupportedException {
    	paul.setPoints(40);
    	paul.setAvantage(true);
    	Field reader = Partie.class.getDeclaredField("joueur1");
    	reader.setAccessible(true);
    	reader.set(partie, paul);
    	partie.gainAvantage(paul);
    	assertFalse(partie.getJoueur1().isAvantage());
    	assertFalse(partie.getJoueur2().isAvantage());
    	assertEquals(1, partie.getJoueur1().getJeux());
    	assertEquals(0, partie.getJoueur2().getJeux());
    }
    
    @Test
    void gainJeu() throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException, CloneNotSupportedException {
    	pierre.setPoints(40);
    	pierre.setAvantage(true);
    	Field reader = Partie.class.getDeclaredField("joueur2");
    	reader.setAccessible(true);
    	reader.set(partie, pierre);
    	partie.gainJeu(pierre);
    	assertFalse(partie.getJoueur2().isAvantage());
    	assertFalse(partie.getJoueur1().isAvantage());
    	assertEquals(1, partie.getJoueur2().getJeux());
    	assertEquals(0, partie.getJoueur1().getJeux());
    }
    
    @Test
    void illegalPlayerSet() {
    	Throwable exception = assertThrows(IllegalArgumentException.class, () -> {partie.gainSet(new Joueur("1"));} );
    	assertEquals("Joueur pas dans la partie!", exception.getMessage());
    }
    
    @Test
    void notEnoughJeuSet() {
    	Throwable exception = assertThrows(IllegalArgumentException.class, () -> {
    		pierre.setPoints(40);
    		pierre.setAvantage(true);
        	Field reader = Partie.class.getDeclaredField("joueur2");
        	reader.setAccessible(true);
        	reader.set(partie, pierre);
    		partie.gainSet(pierre);
    	});
    	assertEquals("Il faut d'abord gagner des jeux!", exception.getMessage());
    }
    
    @Test
    void gainSetFromJeu() throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException, CloneNotSupportedException {
    	paul.setPoints(40);
    	paul.setAvantage(true);
    	paul.setJeux(6);
    	pierre.setJeux(3);
    	Field reader = Partie.class.getDeclaredField("joueur1");
    	reader.setAccessible(true);
    	reader.set(partie, paul);
    	reader = Partie.class.getDeclaredField("joueur2");
    	reader.setAccessible(true);
    	reader.set(partie, pierre);
    	partie.gainJeu(partie.getJoueur1());
    	assertFalse(partie.getJoueur1().isAvantage());
    	assertFalse(partie.getJoueur2().isAvantage());
    	assertEquals(0, partie.getJoueur1().getJeux());
    	assertEquals(0, partie.getJoueur2().getJeux());
    	assertEquals(1, partie.getJoueur1().getSets());
    	assertEquals(0, partie.getJoueur2().getSets());
    }
    
    @Test
    void gainSet() throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException, CloneNotSupportedException {
    	pierre.setPoints(40);
    	pierre.setAvantage(true);
    	pierre.setJeux(6);
    	pierre.setSets(0);
    	paul.setJeux(3);
    	paul.setSets(0);
    	Field reader = Partie.class.getDeclaredField("joueur1");
    	reader.setAccessible(true);
    	reader.set(partie, paul);
    	reader = Partie.class.getDeclaredField("joueur2");
    	reader.setAccessible(true);
    	reader.set(partie, pierre);
    	partie.gainSet(pierre);
    	assertFalse(partie.getJoueur2().isAvantage());
    	assertFalse(partie.getJoueur1().isAvantage());
    	assertEquals(0, partie.getJoueur2().getJeux());
    	assertEquals(0, partie.getJoueur1().getJeux());
    	assertEquals(1, partie.getJoueur2().getSets());
    	assertEquals(0, partie.getJoueur1().getSets());
    }
    
    @Test
    void finPartie() throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException, CloneNotSupportedException {
    	pierre.setPoints(40);
    	pierre.setAvantage(true);
    	pierre.setJeux(6);
    	pierre.setSets(1);
    	paul.setJeux(3);
    	paul.setSets(0);
    	Field reader = Partie.class.getDeclaredField("joueur1");
    	reader.setAccessible(true);
    	reader.set(partie, paul);
    	reader = Partie.class.getDeclaredField("joueur2");
    	reader.setAccessible(true);
    	reader.set(partie, pierre);
    	partie.gainSet(pierre);
    	assertFalse(partie.getJoueur2().isAvantage());
    	assertFalse(partie.getJoueur1().isAvantage());
    	assertEquals(0, partie.getJoueur2().getJeux());
    	assertEquals(0, partie.getJoueur1().getJeux());
    	assertEquals(2, partie.getJoueur2().getSets());
    	assertEquals(0, partie.getJoueur1().getSets());
    }
    
    @Test
    void result() throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
    	pierre.setPoints(40);
    	pierre.setAvantage(true);
    	pierre.setJeux(6);
    	pierre.setSets(1);
    	paul.setJeux(3);
    	paul.setSets(0);
    	Field reader = Partie.class.getDeclaredField("joueur1");
    	reader.setAccessible(true);
    	reader.set(partie, paul);
    	reader = Partie.class.getDeclaredField("joueur2");
    	reader.setAccessible(true);
    	reader.set(partie, pierre);
    	partie.finPartie();
    	assertTrue(partie.isEnded());
    	assertEquals("Paul: 0 / Pierre: 1", partie.result());
    }
    
    @Test
    void jeuDecisif() throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException, CloneNotSupportedException {
    	paul.setJeux(6);
    	pierre.setPoints(40);
    	pierre.setAvantage(true);
    	pierre.setJeux(5);
    	Field reader = Partie.class.getDeclaredField("joueur1");
    	reader.setAccessible(true);
    	reader.set(partie, paul);
    	reader = Partie.class.getDeclaredField("joueur2");
    	reader.setAccessible(true);
    	reader.set(partie, pierre);
    	partie.gainJeu(pierre);
    	partie.gainPoints(partie.getJoueur1());
    	assertEquals(1, partie.getJoueur1().getPoints());
    }
    
    @Test
    void gainJeuDecisif() throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException, CloneNotSupportedException {
    	paul.setJeux(6);
    	paul.setPoints(6);
    	pierre.setJeux(6);
    	pierre.setPoints(5);
    	Field reader = Partie.class.getDeclaredField("joueur1");
    	reader.setAccessible(true);
    	reader.set(partie, paul);
    	reader = Partie.class.getDeclaredField("joueur2");
    	reader.setAccessible(true);
    	reader.set(partie, pierre);
    	partie.jeuDecisif();
    	partie.gainPoints(partie.getJoueur1());
    	assertEquals(0, partie.getJoueur2().getPoints());
    	assertEquals(0, partie.getJoueur1().getPoints());
    	assertFalse(partie.getJoueur2().isAvantage());
    	assertFalse(partie.getJoueur1().isAvantage());
    	assertEquals(0, partie.getJoueur2().getJeux());
    	assertEquals(0, partie.getJoueur1().getJeux());
    	assertEquals(0, partie.getJoueur2().getSets());
    	assertEquals(1, partie.getJoueur1().getSets());
    }
    
    @Test
    void pasGainJeuDecisif() throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException, CloneNotSupportedException {
    	paul.setJeux(6);
    	paul.setPoints(8);
    	pierre.setJeux(6);
    	pierre.setPoints(8);
    	Field reader = Partie.class.getDeclaredField("joueur1");
    	reader.setAccessible(true);
    	reader.set(partie, paul);
    	reader = Partie.class.getDeclaredField("joueur2");
    	reader.setAccessible(true);
    	reader.set(partie, pierre);
    	partie.jeuDecisif();
    	partie.gainPoints(partie.getJoueur2());
    	assertEquals(9, partie.getJoueur2().getPoints());
    	assertEquals(8, partie.getJoueur1().getPoints());
    	assertFalse(partie.getJoueur2().isAvantage());
    	assertFalse(partie.getJoueur1().isAvantage());
    	assertEquals(6, partie.getJoueur2().getJeux());
    	assertEquals(6, partie.getJoueur1().getJeux());
    	assertEquals(0, partie.getJoueur2().getSets());
    	assertEquals(0, partie.getJoueur1().getSets());
    }
    
    @Test
    void apresGainJeuDecisif() throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException, CloneNotSupportedException {
    	paul.setJeux(6);
    	paul.setPoints(6);
    	pierre.setJeux(6);
    	pierre.setPoints(5);
    	Field reader = Partie.class.getDeclaredField("joueur1");
    	reader.setAccessible(true);
    	reader.set(partie, paul);
    	reader = Partie.class.getDeclaredField("joueur2");
    	reader.setAccessible(true);
    	reader.set(partie, pierre);
    	partie.jeuDecisif();
    	partie.gainPoints(partie.getJoueur1());
    	partie.gainPoints(partie.getJoueur1());
    	assertEquals(0, partie.getJoueur2().getPoints());
    	assertEquals(15, partie.getJoueur1().getPoints());
    	assertFalse(partie.getJoueur2().isAvantage());
    	assertFalse(partie.getJoueur1().isAvantage());
    	assertEquals(0, partie.getJoueur2().getJeux());
    	assertEquals(0, partie.getJoueur1().getJeux());
    	assertEquals(0, partie.getJoueur2().getSets());
    	assertEquals(1, partie.getJoueur1().getSets());
    }
    
    @Test
    void PasModificationApresFin() throws CloneNotSupportedException {
    	partie.finPartie();
    	partie.gainPoints(partie.getJoueur1());
    	partie.gainAvantage(partie.getJoueur1());
    	partie.gainJeu(partie.getJoueur1());
    	partie.gainSet(partie.getJoueur1());
    	partie.jeuDecisif();
    	assertEquals(0, partie.getJoueur1().getPoints());
    	assertFalse(partie.getJoueur1().isAvantage());
    	assertEquals(0, partie.getJoueur1().getJeux());
    	assertEquals(0, partie.getJoueur1().getSets());
    	assertFalse(partie.isJeuDecisif());
    	assertTrue(partie.isEnded());
    }
}
